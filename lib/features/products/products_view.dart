import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:question1/data/model/product.dart';
import 'package:question1/data/repository/product_repository.dart';
import 'package:question1/features/products/item/item_product_widget.dart';
import 'package:question1/widgets/indicator/spin_kit_ring.dart';

class ProductsView extends StatefulWidget {
  const ProductsView({Key? key}) : super(key: key);

  @override
  _ProductsViewState createState() => _ProductsViewState();
}

class _ProductsViewState extends State<ProductsView> {
  bool _isLoading = true;
  final List<Product> _products = [];

  @override
  void initState() {
    super.initState();
    ProductRepository.instance.getProducts().then((products) {
      setState(() {
        _products.clear();
        _products.addAll(products);
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: _isLoading
          ? const SpinKitRing(color: Colors.grey)
          : SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(left: 32, top: 16, bottom: 8),
                    child: Text('Products',
                        style: TextStyle(
                            fontSize: 24, fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                      child: ListView.separated(
                          padding: const EdgeInsets.all(16),
                          itemBuilder: (_, index) {
                            final item = _products[index];
                            return ItemProductWidget(
                                key: Key(item.uuid), item: item);
                          },
                          separatorBuilder: (_, __) =>
                              const SizedBox(height: 16),
                          itemCount: _products.length))
                ],
              ),
            ),
    );
  }
}
