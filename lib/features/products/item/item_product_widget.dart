import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:question1/data/model/product.dart';
import 'package:question1/widgets/indicator/network_image.dart' as nw;

class ItemProductWidget extends StatelessWidget {
  final Product item;

  const ItemProductWidget({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final imageWidth = MediaQuery.of(context).size.width / 3;
    return Container(
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
            offset: const Offset(0.0, -2.0),
            blurRadius: 8.0,
            color: Colors.grey.withOpacity(0.15)),
        BoxShadow(
            offset: const Offset(0.0, 2.0),
            blurRadius: 8.0,
            color: Colors.grey.withOpacity(0.15))
      ], color: Colors.white, borderRadius: BorderRadius.circular(16)),
      child: Row(
        children: [
          Container(
            width: imageWidth,
            height: imageWidth,
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                border: Border.all(width: 1, color: Colors.grey.shade200)),
            child: nw.NetworkImage(
                url: item.imageUrls.first,
                width: imageWidth,
                height: imageWidth),
          ),
          SizedBox(width: 16),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(item.name,
                  style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis),
              const SizedBox(height: 8),
              Text(item.description,
                  style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.normal,
                      color: Colors.black),
                  maxLines: 4,
                  overflow: TextOverflow.ellipsis)
            ],
          ))
        ],
      ),
    );
  }
}
