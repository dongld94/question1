import 'package:flutter/material.dart';
import 'package:question1/features/splash/splash_view.dart';

void main() async {
  runApp(const Question1Application());
}

class Question1Application extends StatelessWidget {
  const Question1Application({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        debugShowCheckedModeBanner: false, home: SplashView());
  }
}
