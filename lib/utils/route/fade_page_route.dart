import 'package:flutter/material.dart';

class FadePageRoute<T> extends PageRoute<T> {
  FadePageRoute(this.child, {RouteSettings? settings, this.duration = 300})
      : super(settings: settings);

  @override
  Color get barrierColor => Colors.grey.withOpacity(0.1);

  @override
  String? get barrierLabel => null;

  final Widget child;
  final int duration;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return FadeTransition(
      opacity: animation,
      child: child,
    );
  }

  @override
  bool get maintainState => true;

  @override
  Duration get transitionDuration => Duration(milliseconds: duration);
}
