import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class NetworkImage extends StatelessWidget {
  final String url;
  final double width;
  final double height;

  const NetworkImage(
      {Key? key, required this.url, this.width = 24, this.height = 24})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
        imageUrl: url,
        width: width,
        height: height,
        errorWidget: (_, __, ___) => SizedBox(width: width, height: height),
        placeholder: (context, url) => SizedBox(width: width, height: height));
  }
}
