import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:question1/data/model/product.dart';

class ProductRepository {
  static final ProductRepository _instance = ProductRepository._internal();

  static ProductRepository get instance => _instance;

  ProductRepository._internal();

  Future<List<Product>> getProducts() async {
    await Future.delayed(const Duration(seconds: 1));
    final json = jsonDecode(await rootBundle.loadString('assets/catalog.json'));
    return (json['data'] as List).map((e) => Product.fromJson(e)).toList();
  }
}
