class Product {
  late String uuid;
  late String name;
  late String description;
  late int rank;
  late String status;
  late bool justDropped;
  late List<String> imageUrls;
  late bool isDeleted;
  late String seo;
  late String createdAt;
  late String updatedAt;

  Product(
      {required this.uuid,
      required this.name,
      required this.description,
      required this.rank,
      required this.status,
      required this.justDropped,
      required this.imageUrls,
      required this.isDeleted,
      required this.seo,
      required this.createdAt,
      required this.updatedAt});

  Product.fromJson(Map<String, dynamic> json) {
    uuid = json['uuid'];
    name = json['name'];
    description = json['description'];
    rank = json['rank'];
    status = json['status'];
    justDropped = json['justDropped'];
    imageUrls = (json['imageUrls'] as List).map((e) => e.toString()).toList();
    isDeleted = json['isDeleted'];
    seo = json['seo'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }
}
